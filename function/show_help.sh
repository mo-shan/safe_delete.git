#!/bin/bash
# File Name   : ../function/show_help.sh
# Author      : moshan
# Mail        : mo_shan@yeah.net
# Created Time: 2019-04-15 16:44:16
# Function    : 
#########################################################################
work_dir="/data/git/safe_delete"
function f_show_help()
{
	echo
	echo -e "\033[34m"
	echo "Usage: bash ${script_name} [OPTION]..."
	echo
	echo -e "\033[34m--type=value                      \033[33m可选的值有\"test | get | delete\""
	echo "                                  这个参数的默认值是 \"test\""
	echo "                                  test  ： \"测试MySQL是否正常，检测该MySQL的角色(master | slave)及测试删除语法是否正确\""
	echo "                                  get   ： \"根据查询条件获取到主键列表，并重新拼接生成删除数据的SQL语句示例，这个值包含test的操作\""
	echo "                                  delete： \"重新拼接生成删除数据的SQL语句并实际删除，这个值包含get的操作\""
	echo
	echo -e "\033[34m--table=table_name                \033[33m目标表的表名，仅支持单表，且表名不允许有空格"
	echo
	echo -e "\033[34m--database=db_name                \033[33m目标库的库名，仅支持单库，且库名不允许有空格"
	echo
	echo -e "\033[34m--count=number                    \033[33m指定数字，作为限制删除的行数，类似limit，默认是2000"
	echo
	echo -e "\033[34m--time=number                     \033[33m指定数字，作为每个删除操作的时间间隔，默认是1秒"
	echo
	echo -e "\033[34m--pri-name=id                     \033[33m目标表的主键/非空唯一索引字段的名称，如果是多列索引用逗号隔开，如：--pk-name=pk_1,pk_2,pk_3"
	echo
	echo -e "\033[34m--user=mysql_user                 \033[33m目标MySQL的连接用户名"
	echo
	echo -e "\033[34m--pass=mysql_pass                 \033[33m目标MySQL的连接密码，也可不指定，不指定时会通过交互式输入"
	echo
	echo -e "\033[34m--host=mysql_host                 \033[33m目标MySQL的主机名，可以是IP地址或者域名，如果指定了--sock参数，这个参数会失效"
	echo
	echo -e "\033[34m--port=mysql_port                 \033[33m目标MySQL的端口，如果指定了--sock参数，这个参数会失效"
	echo
	echo -e "\033[34m--sock=mysql_socket_file          \033[33m目标MySQL的socket文件的绝对路径，即便指定了--host和--port参数，也会优先启用这个参数"
	echo
	echo -e "\033[34m--where=\"col = 'str'\"             \033[33m目标表的删除条件，对于字符串部分请用单引号引用，请勿用双引号"
	echo
	echo -e "\033[34m--help or -h                      \033[33m展示帮助手册"
	echo -e "\033[0m"
	echo -e "\033[35m注：如需要安全停止程序，请在日志目录下创建一个stop_file文件，该操作会在当前删除操作执行完毕后退出程序。如：[\033[33mtouch ${log_dir}/stop_file\033[35m]\033[0m"
	echo
}

