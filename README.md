# safe_delete

### 一、介绍

工具用于安全删除MySQL表，对于一些特定场景可能有用，欢迎吐槽。

应用场景：大批删除不走索引但是有主键的场景[可以是单列索引，也可是多列索引]。

使用要求：必须有主键或者非空唯一索引。

实现思路：根据where条件获取到主键值，然后重新拼接SQL。

> 注：本工具仅用于学习，如用于生产，请充分测试。


### 二、安装教程

#### 1. 克隆项目

```
git clone https://gitee.com/mo-shan/safe_delete.git
```
#### 2. 配置软件
```
sed -i 's#^mysql_path=.*#mysql_path=\"/mysqlbinlog_path\"#g' bin/safe_delete #将这里的mysql_path改成mysql工具的绝对路径,否则可能会因版本太低导致错误
sed -i 's#^work_dir=.*#work_dir=\"/safe_delete_path\"#g' bin/safe_delete #将这里的safe_delete_path改成safe_delete的家目录的绝对路径
```
>  注: 也可以打开文件编辑[vim bin/safe_delete]，根据实际情况更新8 9 两行配置


### 三、使用说明

#### 1、帮助手册

```

moshan /data/git/safe_delete/function > sudo bash ../bin/safe_delete -h

Usage: bash safe_delete [OPTION]...

--type=value                      可选的值有"test | get | delete"
                                  这个参数的默认值是 "test"
                                  test  ： "测试MySQL是否正常，检测该MySQL的角色(master | slave)及测试删除语法是否正确"
                                  get   ： "根据查询条件获取到主键列表，并重新拼接生成删除数据的SQL语句示例，这个值包含test的操作"
                                  delete： "重新拼接生成删除数据的SQL语句并实际删除，这个值包含get的操作"

--table=table_name                目标表的表名，仅支持单表，且表名不允许有空格

--database=db_name                目标库的库名，仅支持单库，且库名不允许有空格

--count=number                    指定数字，作为限制删除的行数，类似limit，默认是2000

--time=number                     指定数字，作为每个删除操作的时间间隔，默认是1秒

--pri-name=id                     目标表的主键/非空唯一索引字段的名称，如果是多列索引用逗号隔开，如：--pk-name=pk_1,pk_2,pk_3

--user=mysql_user                 目标MySQL的连接用户名

--pass=mysql_pass                 目标MySQL的连接密码，也可不指定，不指定时会通过交互式输入

--host=mysql_host                 目标MySQL的主机名，可以是IP地址或者域名，如果指定了--sock参数，这个参数会失效

--port=mysql_port                 目标MySQL的端口，如果指定了--sock参数，这个参数会失效

--sock=mysql_socket_file          目标MySQL的socket文件的绝对路径，即便指定了--host和--port参数，也会优先启用这个参数

--where="col = 'str'"             目标表的删除条件，对于字符串部分请用单引号引用，请勿用双引号

--help or -h                      展示帮助手册

注：如需要安全停止程序，请在日志目录下创建一个stop_file文件，该操作会在当前删除操作执行完毕后退出程序。如：[touch /data/git/safe_delete/log/stop_file]


moshan /data/git/safe_delete > 
```


#### 2、使用演示

- 环境信息：

```
mysql(test@localhost mysqld.sock)> show create table wechat_binding \G
*************************** 1. row ***************************
       Table: wechat_binding
Create Table: CREATE TABLE `wechat_binding` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户ID',
  `sales_id` varchar(80) DEFAULT NULL COMMENT '销售编号',
  `open_id` varchar(45) NOT NULL COMMENT 'openID',
  `type` int(2) NOT NULL COMMENT '1 销售，2 用户',
  `phone` varchar(20) NOT NULL COMMENT '手机号',
  `create_time` datetime NOT NULL COMMENT '绑定时间',
  PRIMARY KEY (`id`,`open_id`,`type`),
  KEY `openIdIdx` (`open_id`) USING BTREE,
  KEY `phoneIdx` (`phone`) USING BTREE,
  KEY `salesIdIndex` (`sales_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5000001 DEFAULT CHARSET=utf8
1 row in set (0.03 sec)

mysql(test@localhost mysqld.sock)> 

```

> 需求：删除手机号是“00175731528296189904”的数据，手机号不是索引字段。

- （1）test的功能
```
moshan /data/git/safe_delete >> sudo bash bin/safe_delete --table=wechat_binding --database=test --count=2000 --time=1 --pri-name=id,open_id,type --where="phone='00175731528296189904'"  --user=test --host=127.0.0.1 --port=3311 --type=test

[2019-08-12 15:28:07] [WARN] [172.28.84.239] 请输入MySQL的密码：******
[2019-08-12 15:28:09] [INFO] [172.28.84.239] MySQL连接正常!
[2019-08-12 15:28:09] [INFO] [172.28.84.239] 这个MySQL是Master!
moshan /data/git/safe_delete >>
```

- （2）get的功能
```
moshan /data/git/safe_delete > sudo bash bin/safe_delete --table=wechat_binding --database=test --count=2000 --time=1 --pri-name=id,open_id,type --where="phone='00175731528296189904'"  --user=test --host=127.0.0.1 --port=3311 --type=get

[2019-08-12 15:28:01] [WARN] [172.28.84.239] 请输入MySQL的密码：******
[2019-08-12 15:28:03] [INFO] [172.28.84.239] MySQL连接正常!
[2019-08-12 15:28:03] [INFO] [172.28.84.239] 这个MySQL是Master!
[2019-08-12 15:28:03] [INFO] [172.28.84.239] 删除语法正确，[delete from wechat_binding where (phone='00175731528296189904') and (id,open_id,type) = (52,'005345534517573152829618990428832934940817775',17);]
moshan /data/git/safe_delete > 

```

- （3）delete的功能
```
moshan /data/git/safe_delete >  sudo bash bin/safe_delete --table=wechat_binding --database=test --count=2000 --time=1 --pri-name=id,open_id,type --where="phone='00175731528296189904'"  --user=test --host=127.0.0.1 --port=3311 --type=delete 
[2019-08-12 15:28:14] [WARN] [172.28.84.239] 请输入MySQL的密码：******
[2019-08-12 15:28:16] [INFO] [172.28.84.239] MySQL连接正常!
[2019-08-12 15:28:16] [INFO] [172.28.84.239] 这个MySQL是Master!
[2019-08-12 15:28:16] [INFO] [172.28.84.239] 删除语法正确，[delete from wechat_binding where (phone='00175731528296189904') and (id,open_id,type) = (52,'005345534517573152829618990428832934940817775',17);]
[2019-08-12 15:28:16] [INFO] [172.28.84.239] 开始本次删除, 预计总共需要删除50430行数据, 需要删除26次......
[2019-08-12 15:28:16] [INFO] [172.28.84.239] 开始第1次删除......
[2019-08-12 15:28:18] [INFO] [172.28.84.239] 第1次删除完成, 删除的行数:2000
[2019-08-12 15:28:19] [INFO] [172.28.84.239] 开始第2次删除......
[2019-08-12 15:28:21] [INFO] [172.28.84.239] 第2次删除完成, 删除的行数:2000
[2019-08-12 15:28:22] [INFO] [172.28.84.239] 开始第3次删除......
[2019-08-12 15:28:24] [INFO] [172.28.84.239] 第3次删除完成, 删除的行数:2000
[2019-08-12 15:28:25] [INFO] [172.28.84.239] 开始第4次删除......
[2019-08-12 15:28:26] [INFO] [172.28.84.239] 第4次删除完成, 删除的行数:2000
[2019-08-12 15:28:27] [INFO] [172.28.84.239] 开始第5次删除......
[2019-08-12 15:28:29] [INFO] [172.28.84.239] 第5次删除完成, 删除的行数:2000
[2019-08-12 15:28:30] [INFO] [172.28.84.239] 开始第6次删除......
[2019-08-12 15:28:33] [INFO] [172.28.84.239] 第6次删除完成, 删除的行数:2000
[2019-08-12 15:28:34] [INFO] [172.28.84.239] 开始第7次删除......
[2019-08-12 15:28:37] [INFO] [172.28.84.239] 第7次删除完成, 删除的行数:2000
[2019-08-12 15:28:38] [INFO] [172.28.84.239] 开始第8次删除......
[2019-08-12 15:28:40] [INFO] [172.28.84.239] 第8次删除完成, 删除的行数:2000
[2019-08-12 15:28:41] [INFO] [172.28.84.239] 开始第9次删除......
[2019-08-12 15:28:44] [INFO] [172.28.84.239] 第9次删除完成, 删除的行数:2000
[2019-08-12 15:28:45] [INFO] [172.28.84.239] 开始第10次删除......
[2019-08-12 15:28:47] [INFO] [172.28.84.239] 第10次删除完成, 删除的行数:2000
[2019-08-12 15:28:48] [INFO] [172.28.84.239] 开始第11次删除......
[2019-08-12 15:28:49] [INFO] [172.28.84.239] 第11次删除完成, 删除的行数:2000
[2019-08-12 15:28:50] [INFO] [172.28.84.239] 开始第12次删除......
[2019-08-12 15:28:53] [INFO] [172.28.84.239] 第12次删除完成, 删除的行数:2000
[2019-08-12 15:28:54] [INFO] [172.28.84.239] 开始第13次删除......
[2019-08-12 15:28:56] [INFO] [172.28.84.239] 第13次删除完成, 删除的行数:2000
[2019-08-12 15:28:57] [INFO] [172.28.84.239] 开始第14次删除......
[2019-08-12 15:28:59] [INFO] [172.28.84.239] 第14次删除完成, 删除的行数:2000
[2019-08-12 15:29:00] [INFO] [172.28.84.239] 开始第15次删除......
[2019-08-12 15:29:02] [INFO] [172.28.84.239] 第15次删除完成, 删除的行数:2000
[2019-08-12 15:29:03] [INFO] [172.28.84.239] 开始第16次删除......
[2019-08-12 15:29:05] [INFO] [172.28.84.239] 第16次删除完成, 删除的行数:2000
[2019-08-12 15:29:06] [INFO] [172.28.84.239] 开始第17次删除......
[2019-08-12 15:29:08] [INFO] [172.28.84.239] 第17次删除完成, 删除的行数:2000
[2019-08-12 15:29:09] [INFO] [172.28.84.239] 开始第18次删除......
[2019-08-12 15:29:11] [INFO] [172.28.84.239] 第18次删除完成, 删除的行数:2000
[2019-08-12 15:29:12] [INFO] [172.28.84.239] 开始第19次删除......
[2019-08-12 15:29:14] [INFO] [172.28.84.239] 第19次删除完成, 删除的行数:2000
[2019-08-12 15:29:15] [INFO] [172.28.84.239] 开始第20次删除......
[2019-08-12 15:29:17] [INFO] [172.28.84.239] 第20次删除完成, 删除的行数:2000
[2019-08-12 15:29:18] [INFO] [172.28.84.239] 开始第21次删除......
[2019-08-12 15:29:20] [INFO] [172.28.84.239] 第21次删除完成, 删除的行数:2000
[2019-08-12 15:29:21] [INFO] [172.28.84.239] 开始第22次删除......
[2019-08-12 15:29:23] [INFO] [172.28.84.239] 第22次删除完成, 删除的行数:2000
[2019-08-12 15:29:24] [INFO] [172.28.84.239] 开始第23次删除......
[2019-08-12 15:29:26] [INFO] [172.28.84.239] 第23次删除完成, 删除的行数:2000
[2019-08-12 15:29:28] [INFO] [172.28.84.239] 开始第24次删除......
[2019-08-12 15:29:30] [INFO] [172.28.84.239] 第24次删除完成, 删除的行数:2000
[2019-08-12 15:29:31] [INFO] [172.28.84.239] 开始第25次删除......
[2019-08-12 15:29:33] [INFO] [172.28.84.239] 第25次删除完成, 删除的行数:2000
[2019-08-12 15:29:34] [INFO] [172.28.84.239] 开始第26次删除......
[2019-08-12 15:29:34] [INFO] [172.28.84.239] 第26次删除完成, 删除的行数:430
[2019-08-12 15:29:35] [INFO] [172.28.84.239] 本次删除完成，总共删除的行数:50430

moshan /data/git/safe_delete > 

```